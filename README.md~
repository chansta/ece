# Event Count Estimation

## Introduction

This repository contains some of the code used in the paper "Event Count Estimation" by Laszlo Balazsi, Felix Chan and Laszlo Matyas. The repository contains implementations of four variants of the Event Count Estimator (ECE). The [R][R] code contains the restricted and unrestricted ECE while the [Python][python] code implements the Conditional ECE as well as its non-parametric version. The Python object also contians a Monte Carlo routine used in the paper.  

## R Requirements
The following libraries are required for the [R][R] implementation

    - [dplyr](https://cran.r-project.org/web/packages/dplyr/index.html)
    - [expm](https://cran.r-project.org/web/packages/expm/index.html)
    - [gmm](https://cran.r-project.org/web/packages/gmm/index.html)
    - [MASS](https://cran.r-project.org/web/packages/MASS/index.html)
    - [Matrix](https://cran.r-project.org/web/packages/Matrix/index.html)
    - [nloptr](https://cran.r-project.org/web/packages/nloptr/index.html)

## Python Requirements

The Python module requires the following:

    - [datetime](https://docs.python.org/3/library/datetime.html)
    - [h5py](https://www.h5py.org/)
    - [matplotlib](https://matplotlib.org/)
    - [numpy](https://numpy.org/)
    - [scipy](https://www.scipy.org/)
    - [statsmodels](https://www.statsmodels.org/stable/index.html)

[R]: http://www.r-project.org
[python]: http://www.python.org


