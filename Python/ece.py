##################################################################################################
"""
name:				ece.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2017.12.02
description:			Python library for Event Count Estimator (ECE). It includes Monte Carlo and estimation routine. 
"""
##################################################################################################
import numpy as np
import scipy as sp
import scipy.stats as sps
import scipy.optimize as spo
import matplotlib.pyplot as plt
import h5py as h5
import datetime as dt
import statsmodels.nonparametric.kernel_density as sm_kde

class cece(object):
    """
	Defining cece object for puspose of estimation. It implements the Conditional Event Count Estimator (CECE). 
    """
    def __init__(self, y, x, trunc='default', constant=True): 
        """
            y: (N,) numpy array. NX1 vector containing the data for the endogeneous variable. 
            x: (N,k) numpy array. NXk vector containing the data for the explanatory variables. 
            trunc: either 'default' or (2,) list containing the upper and lower bounds for the ECE estimator. 
            constant: Boolean. If true, estimation includes a constant.	
            Members:
                self.y: object copy of y. 
                self.x: object copy of x if constant is false and [1,x] if constant is true.
                self.N: the number of observations. 
                self.k: the number of explanatory variables. 
                self.K: the number of parameters. self.K = self.k if no constant otherwise self.K = self.k+1
                self.trunc: object copy of trunc. If self.trunc is numeric, it means the self.bounds have been manually changed since the initiation of the instance. 
                self.bounds: the upper and lower bounds of the conditional ECE estimator. 
                self.constant: object copy of constant. 
        """
        self.y = y
        self.N, self.k = x.shape
        if constant is False:
            self.x = x
            self.K = self.k
        else:
            self.x = np.c_[np.ones((self.N,1)), x] 
            self.K = self.k + 1
        self.trunc = trunc 
        if self.trunc == 'default':
            self.s = ols_variance(self.y, self.x)
        else:
            self.bounds = trunc

    def cece_norm(self, b, hetero=True, objective=True):
        """
        Return the conditional expectation of d(u) based on 
        Inputs:
            b: (k,) numpy array. Vector of parameters. 
            hetero: boolean. If True, the variance is assumed to be different for each observations will be estiamted by the squared of error for each individual. Otherwise, it will assume to be fixed and estimated by the standard variance estimator. 
            objective: boolean. See outputs. 
        Outputs:
            If objective is true, 
                then the function returns a scalar of the average of the conditonal expectation
            else
                the function returns a (N,) numpy array of conditional expectation. 
        
        """
        
        e = self.y - np.dot(self.x, b).reshape((self.N,1))
        e = e.reshape(self.N)
        if hetero is True:
            sigma2 = e*e
        else:
            sigma2 = np.dot(e,e) 
        sigma = np.power(sigma2, 0.5)
        L = 0.5*(sps.norm.cdf(self.bounds[1], scale=sigma) - sps.norm.cdf(self.bounds[0], scale=sigma))
        if objective is True:
            return np.mean(L)
        else:
            return L 
    
    def cece_UR(self,b):
        """
            Implememnt the Unrestricted ECE estimator. 
        """
        e = self.y - np.dot(self.x, b).reshape((self.N,1))
        di = (e>self.bounds[0])*(e<self.bounds[1])
        d = float(sum(di)/self.N)
        return d

    def cece_nonparam(self,b, objective=True):
        """
            Implement the Nonparametric estimator. 
        """
        z = np.dot(self.x,b)
        nonparam = sm_kde.KDEMultivariateConditional(endog=self.y.reshape(self.N), exog=z.reshape(self.N), dep_type='c', indep_type='c', bw='normal_reference')
        J = np.zeros(self.N)
        for i,u in enumerate(z): 
            U_den = 1 - nonparam.cdf(endog_predict=[u], exog_predict=[u])[0]
            U_num = nonparam.cdf(endog_predict=[self.bounds[1]+u], exog_predict=[u])[0] - nonparam.cdf(endog_predict=[u], exog_predict=[u])[0]
            L_den = nonparam.cdf(endog_predict=[u], exog_predict=[u])[0]
            L_num = -nonparam.cdf(endog_predict=[self.bounds[0]+u], exog_predict=[u])[0] + nonparam.cdf(endog_predict=[u], exog_predict=[u])[0]
            J[i] = U_num/U_den + L_num/L_den
        if objective is True:
            return np.mean(J)
        else:
            return J

    def __set_bounds__(self, estimator='cece', bounds=None, external=True):
        """
            Setting the boundary values based on either self.trunc or the estimator. 
            Input:
                    estimator:  'cece', 'nonparam', 'R' and 'UR'. Conditional ECE, Nonparametric, Restructed and Unrestricted. 
                    bounds: 2-tuple. Default: None. If None default boundary will be used based on the estimator input. Otherwise, it uses the proposed bounds. 
                    external: Boolean. 0 means the function is being called interally, self.trunc keeps its values otherwise self.trunc == self.bounds. 
        """
        if external == True:
            self.trunc = 0 
        if bounds is not None:
            self.bounds = bounds
        else:
            s = np.power(self.s, 0.5)
            if (estimator=='cece')&(self.trunc=='default'):
                self.bounds = [-self.s/np.sqrt(self.N), self.s/np.sqrt(self.N)]
            elif (estimator=='cece_homo')&(self.trunc=='default'): 
               # self.bounds = [-s*1.64, s*1.64]
                self.bounds = [-self.s/np.sqrt(self.N), self.s/np.sqrt(self.N)]
            elif (estimator == 'UR')&(self.trunc == 'default'):
                self.bounds = [-s*1.64, s*1.64]
            elif (estimator == 'nonparam')&(self.trunc == 'default'):
                sy = np.power(np.var(self.y),0.5)
                self.bounds = [-sy/2, sy/2]

    def estimate(self, b0, estimator='cece', hetero=False): 
        """
            Estimate the parameter based on the choice of estimator. (Only CECE is implemented at the moment). 
            Inputs:
                b0: (self.K,) vector of initial parameters. 
                estimator: 'cece', 'cece_homo', 'nonparam', 'R' and 'UR'. Conditional ECE assuming heteroskedasticity, Conditional ECE assuming homoskedasticity, Nonparametric, Restructed and Unrestricted. 
                hetero: Boolean. Relevant for CECE only. 
            
        """
        self.__set_bounds__(estimator=estimator, external=False) 
        if estimator == 'cece':
            obj = lambda b: -self.cece_norm(b, objective=True)
            self.result = spo.minimize(obj, b0, method='BFGS', options={'disp':True})
            self.b = self.result['x']
        elif estimator == 'cece_homo':
            obj = lambda b: -self.cece_norm(b, objective=True, hetero=False)
            self.result = spo.minimize(obj, b0, method='BFGS', options={'disp':True})
            self.b = self.result['x']
        elif estimator == 'UR':
            obj = lambda b: -self.cece_UR(b)
            self.result = spo.basinhopping(obj,b0, niter=1000)
            self.b = self.result['x']
        elif estimator == 'nonparam':
            obj = lambda b: -self.cece_nonparam(b)
            self.result = spo.minimize(obj,b0,options={'disp':True})
            self.b = self.result['x']
        else:
            print('Estimator has not been implemented yet')

class MC_ECE(object): 
    """
        Object to conduct Monte Carlo experiments with ECE estimators.  
        Members:
            self.b: object copy of b.
            self.N: object copy of N.
            self.replication: object copy of replication.
            self.estimator: object copy of estimator. 
            self.udist: object copy of udist.
            self.xdist: object copy of xdist. 
            self.constant: object copy of constant. 
            self.outliers: object copy of outliers. 
            self.k: The number of exaplnatory variables.
            self.K: THe number of parameters. 
            self.result_ols: (replication, self.K) numpy array containing the simulation result under OLS estimator. 
            self.result_cec: (replication, self.K) numpy array containing the simulation result under Conditional ECE estimator. 
    """
    def __init__(self, b, N, replication, estimator='cece', udist='norm', xdist='U', constant=True, outliers=0): 
        """
            b: (k,) numpy array containing the k parameter vector. 
            N: int. Number of observations to be generated in each replication. 
            replication: int. Number of replications. 
            estimator: 'cece', 'nonparam', 'R', 'UR', 'All' - one estimator at a time or all of them. 
            udist: 't5', 'norm' or 'skewnorm'. The distribution of the residual. 
            xdist: 'norm' or 'U'. The distribution of the residual. 
            constant: boolean. Adding a constant if true. The first element in b is assumed to be the coefficient in that case. 
            outliers: [0,1]. Add outliers in specified fraction of the data. 
        """
        self.estimator_list = ['cece', 'cece_homo', 'UR', 'nonparam']
        self.Ne = len(self.estimator_list)+1 
        self.b = b
        self.N = N
        self.K = self.b.shape[0] 
        self.replication = replication
        self.estimator = estimator 
        self.udist = udist
        self.xdist = xdist
        self.constant = constant 
        self.outliers = outliers
        if self.constant is True:
            self.k = self.K-1
        else:
            self.k = self.K
        if estimator == 'All':
            self.result_ece = np.zeros((self.replication,self.Ne, self.K))
        else:
            self.result_ece = np.zeros((self.replication, 2, self.K))

    def simu_linear_t(self):
        """
        Simulate simple linear model y = b*x + u where u based on different distribution. 
        Inputs:
            b: (k,) numpy array of predefine parameter vector. 
            period: int. Number of observations to simulate. 
                    constant: boolean. If true, it will add a constant. 
        Outputs:
            y,x
            y: (N,) numpy array of simulated dependent variable. 
            x: (N,) numpy array of simulated explanatory variable. 
        """

        if self.xdist == 'norm':
            x = sps.norm.rvs(size=(self.N,self.k))
        elif self.xdist == 'U':
            x = sps.uniform.rvs(size=(self.N,self.k), loc=0, scale=10)
        else:
            print('The distribution has not been implemented yet.')
            exit()
        if self.udist == 't5':
            u = sps.t.rvs(size=(self.N,1), df=5)
        elif self.udist == 'norm':
            u = sps.norm.rvs(size=(self.N,1))
        elif self.udist == 'skewnorm':
            u = gen_skewnorm(size=(self.N,1), frac=0.8, scale=1)
        if self.constant == True:
            x = np.c_[np.ones((self.N,1)), x.transpose()[0:self.k].transpose()]
        if self.outliers != 0:
            NN = int(np.floor(self.outliers*self.N))
            index = np.random.choice(np.arange(0,self.N), size=NN, replace=False)
            u[index] = u[index] + 100
        y = np.dot(x, self.b) + u    
        return y,x,u

    def simu_trunc_linear(self, a, zeros=False):
        """
        Simulate truncated simple linear model y = b*x + u where u based on different distribution. 
        Inputs:
            a: float. Threshold value. 
            zeros: boolean. If True, then y will be set to 0 when its value is less than a. If False, then all y valnes that are less than a will be equal to a.
        Outputs:
            y,x, u
            y: (N,) numpy array of simulated dependent variable. 
            x: (N,) numpy array of simulated explanatory variable. 
            u: (N, ) numpy array of simulated residuals.
        """
        
        ys,x,u = self.simu_linear_t()
        if zeros is False:
            c = a
        else:
            c = 0
        y = (ys>a)*ys + c*(ys<a)
        return y,x,u

    def MC_ECE_symmetric(self, savefilename='symmetric.hdf5'): 
        """
            MC for comparing OLS and Conditional ECE.  
        """
        for i in np.arange(0,self.replication):
            print('This is replication {0} out of {1}'.format(i+1, self.replication))
            y,x,u = self.simu_linear_t()
            self.result_ece[i] = self.MC_estimate(y,x) 
        self.save_result(savefilename)

    def MC_ECE_censored(self, a, zeros=False, savefilename='censored.hdf5'): 
        """
            MC for comparing OLS and Conditional ECE under censored case. 
        """
        for i in np.arange(0,self.replication):
            print('This is replication {0} out of {1}'.format(i+1, self.replication))
            y,x,u = self.simu_trunc_linear(a, zeros=zeros)
            self.result_ece[i] = self.MC_estimate(y,x) 
        self.save_result(savefilename)

    def MC_estimate(self, y, x): 
        temp_cece = cece(y,x,constant=self.constant)
        if self.estimator != 'All': 
            temp_cece.estimate(self.b, estimator=self.estimator)
            return np.r_[temp_cece.b.reshape((1, self.K)), ols(y,x).reshape((1,self.K))]
        else:
            temp_result = np.zeros((self.Ne,self.K))
            for j,e in enumerate(self.estimator_list):
                temp_cece.estimate(self.b, estimator=e)
                temp_result[j] = temp_cece.b.reshape((1,self.K))
            temp_result[j+1] = ols(y,x).reshape((1,self.K))
            return temp_result

    def save_result(self, savefilename = 'default.hdf5'):
        """
            Save MC results into specificed filename. 
        """
        result = h5.File(savefilename, 'w') 
        group = result.create_group('MC_result')
        group.attrs['Date'] = dt.datetime.now().isoformat()
        group.attrs['N'] = self.N
        group.attrs['replication'] = self.replication 
        group.attrs['K'] = self.K
        group.attrs['b'] = self.b 
        group.attrs['xdist'] = self.xdist
        group.attrs['udist'] = self.udist
        if self.estimator == 'All':
            estimator_list = self.estimator_list + ['ols']
        else:
            estimator_list = [self.estimator, 'ols']
        for i,e in enumerate(estimator_list):
            group.create_dataset(e, data=self.result_ece[:,i,:])
        result.close()

def ols(y,x):
    """
        OLS estimator of regressing x on y. 
        Inputs:
            y: (T,1) numpy array. NX1 vector containing the data of the endogemous variable. 
            x: (T,K) numpy array. NXK vector containing the data of the explanatory variable. 
        Output:
            A (K,1) vector of OLS estimates. 
    """
    Xinv = np.linalg.inv(np.dot(x.transpose(),x))
    return np.dot(Xinv,np.dot(x.transpose(),y))

def ols_variance(y,x):
    """
    Calculate the OLS residuals variance based on data (y,x)
    Inputs:
            y: (T,1) numpy array. NX1 vector containing the data of the endogemous variable. 
            x: (T,K) numpy array. NXK vector containing the data of the explanatory variable. 
        Output:
            scalar: OLS estimated residual variance.  
    """
    N,k = x.shape
    Xinv = np.linalg.inv(np.dot(x.transpose(),x))
    M = np.eye(N) - np.dot(x, np.dot(Xinv,x.transpose()))
    return float(np.dot(y.transpose(), np.dot(M, y)))/N

def gen_skewnorm(size=1, loc = 0, scale=1, frac=0.5, skew='left'):
    """
        Random generation of a skewed normal distribution as defined in Azzalini based on mean (loc) and standard deviation (scale). 
        Inputs:
            size: int or tuple of of int. The size of random matrix to be generated. 
            loc: float. The mean of the random variable. 
            scale: positive float. The standard deviation of the random variable. 
            frac: float in (0,1). Fraction of scale-squared to be used as the variance of standard normal. 
            skew: 'left' or 'right'. Specifying left or right skew.
        Output:
            Random draw from a specified skewed normal. 
    """

    s = np.power(scale,2)
    sn = s*frac
    l = np.power(sn*(np.pi/(np.pi-2)), 0.5)
    if skew=='left':
        l = -l
    xi = loc - l*np.power(2/np.pi, 0.5)
    N = sps.norm.rvs(size=size, loc=0, scale=scale)
    HN = sps.norm.rvs(size=size)
    HN = abs(HN)
    y = xi + l*HN + N 
    return y 

def mergedata(from_file, to_file, d1_groupname='MC_result'):
    """
        Extract individual MC results from HDF5 file into another file. 
    Inputs:
        from_file: str. Source file name. 
        to_file: str. Destination file name. 
        groupname: str. The name of the top group in from_file
    """
    scenario = from_file.split('/')[-1].split('_')[2]
    m = h5.File(to_file, 'a')
    d1 = h5.File(from_file, 'r')
    d1_g = d1[d1_groupname]
    N = d1_g.attrs['N']
    dist = d1_g.attrs['udist']
    groupname = scenario+'_'+dist+'_'+str(N)
    m.create_group(groupname)
    m_g = m[groupname]
    m_g.attrs['N'] = N
    m_g.attrs['scenario'] = scenario
    for i,n in enumerate(d1_g.keys()):
        m_g.create_dataset(n, data=np.array(d1_g[n]))
    for i,a in enumerate(d1_g.attrs.keys()):
        m_g.attrs[a] = d1_g.attrs[a]
    m.close()
    d1.close()
